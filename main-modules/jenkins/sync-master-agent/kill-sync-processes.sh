#!/bin/bash

# Kill all processes matching the full name of the script
pkill -f /home/docker/sync-logs-between-master-2-slave.sh

echo "All processes of sync-logs was terminated."
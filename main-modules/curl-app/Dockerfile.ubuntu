# Use the latest Ubuntu as the base image
FROM ubuntu:latest


# Install required packages
RUN apk update update && apk add --update \
  build-essential \
  libtool \
  autoconf \
  automake \
  libssl-dev \
  openssl \
  ssh \
  ca-certificates \
  curl \
  gnupg \
  lsb-release \ 
  m4 \
  perl\
  python3




# Add Docker's official GPG key
RUN mkdir -p /etc/apt/keyrings
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg

# Set up the Docker repository
RUN echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install Docker Engine
RUN apk add --update -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Create directories for logs and artifacts
RUN mkdir -p /logs/make_logs /logs/make_test_logs /artifacts

# Set permissions for the directories
RUN chown -R root:root /logs /artifacts

# Set the working directory
WORKDIR /app

# Define the entry point script
COPY entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

